#!/usr/bin/env bash

GODOT_FILE_NAME=Godot_v3.2-stable_osx.64.zip

mkdir temp
wget -P temp https://downloads.tuxfamily.org/godotengine/3.2/${GODOT_FILE_NAME}
cd temp
unzip -o ${GODOT_FILE_NAME}
Godot.app/Contents/MacOS/Godot --quiet --no-window --path ../../../src -q