# Project Name

Project Description

### Todo

- [ ] Grid overlay for controls (?)  
- [ ] Bigger popups for start & IAP  
- [ ] Notifications for IAP  

### In Progress

- [ ] IAP for iOS  

### Done ✓

- [x] IAP for ANdroid  
- [x] IAP base  
- [x] Icon for Android  

