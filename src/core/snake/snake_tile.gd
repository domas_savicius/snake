extends Node2D

var _position_in_grid: Vector2 setget set_position_in_grid, get_position_in_grid;
var _previous_position_in_grid: Vector2 setget ,get_previous_position_in_grid;
var is_head = false;

func get_previous_position_in_grid():
	return _previous_position_in_grid;

func set_position_in_grid(pos: Vector2):
	_previous_position_in_grid = _position_in_grid;
	_position_in_grid = pos;

func get_position_in_grid():
	return _position_in_grid;