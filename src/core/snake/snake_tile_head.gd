extends "res://core/snake/snake_tile.gd"

onready var _sprite = $Sprite;
var _sideways_texture = preload("res://core/snake/assets/snake_tile_head.png");
var _updown_texture = preload("res://core/snake/assets/snake_tile_head_up.png");

func _ready():
	is_head = true;

func set_direction(direction: Vector2):
	if direction == Vector2.UP || direction == Vector2.DOWN:
		_sprite.texture = _updown_texture;
	if direction == Vector2.LEFT || direction == Vector2.RIGHT:
		_sprite.texture = _sideways_texture;