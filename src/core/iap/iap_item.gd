extends Node

class_name IapItem

var id: String;
var price: String;
var price_amount: float;
var currency_code: String;