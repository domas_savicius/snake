extends Node

var _logger = Log.get_logger("in_app_purchase_manager.gd");
const _product_name = "snek_maze_unlock";

var _products_loaded: bool = false;

var _iap_service;
var _items_available = [];

func _ready() -> void:
	var iap = iOSIAP.new();

	if iap.is_supported():
		_init_ios(iap);
		return;

	iap = AndroidIAP.new();
	if iap.is_supported():
		_init_android(iap);
		return;

	_logger.info("In-app purchases not supported");

func get_items():
	return _items_available;

func purchase(id: String):
	_iap_service.purchase(id);

func purchase_levels():
	purchase(_product_name);

func is_supported():
	return _iap_service != null;

# -> Android ------------------------------------------------------------------
func _init_android(iap):
	_iap_service = iap;
	_iap_service.set_auto_consume(false);
	_connect_android_signals();
	_logger.info("Initialized Android IAP");

func _connect_android_signals():
	_iap_service.connect("sku_details_complete", self, "sku_details_complete");
	_iap_service.connect("sku_details_error", self, "sku_details_error");
	_iap_service.connect("purchase_success",self,"android_purchase_success");
	_iap_service.connect("purchase_fail",self,"android_purchase_fail");
	_iap_service.connect("purchase_cancel",self,"android_purchase_cancel");
	_iap_service.connect("purchase_owned",self,"android_purchase_owned");
	_iap_service.connect("has_purchased", self, "android_has_purchased");

func sku_details_complete():
	for product_key in _iap_service.sku_details:
		var item = IapItem.new();
		var product = _iap_service.sku_details[product_key];

		item.id = product["product_id"];
		item.price_amount = product["price_amount"];
		item.currency_code = product["price_currency_code"];
		item.price = product["price"];

		_items_available.append(item);

	_products_loaded = true;

func sku_details_error():
	_logger.error("Sku details error");

func android_purchase_success(_item):
	Events.emit_purchase_success(_product_name);
	NotificationHelper.show("Success!");
	_logger.info("Purchase success %s" %_item);

func android_purchase_fail():
	Events.emit_purchase_fail(_product_name);
	NotificationHelper.show("Failed");
	_logger.info("Purchase fail");

func android_purchase_cancel():
	Events.emit_purchase_cancel(_product_name);
	NotificationHelper.show("Cancelled");
	_logger.info("Purchase cancel");

func android_purchase_owned(_item):
	#TODO owned
	Events.emit_purchase_success(_product_name);
	NotificationHelper.show("Already owned");
	_logger.info("Purchase owned");

func android_has_purchased(item):
	if item == _product_name:
		android_purchase_success(item);
# END

# -> iOS ----------------------------------------------------------------------
func _init_ios(iap):
	_iap_service = iap;
	_connect_ios_signals();
	_logger.info("Initialized iOS IAP");

func _connect_ios_signals():
	_iap_service.connect("purchase_success", self, "ios_purchase_success");
	_iap_service.connect("purchase_fail", self, "ios_purchase_fail");

func ios_purchase_success(product_id):
	Events.emit_purchase_success(_product_name);
	NotificationHelper.show("Success!");
	_logger.info("Purchase success %s" %product_id);

func ios_purchase_fail():
	Events.emit_purchase_fail(_product_name);
	NotificationHelper.show("Failed");
	_logger.info("Purchase fail");
# END