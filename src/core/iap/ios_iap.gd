extends Node

class_name iOSIAP

signal purchase_success(item_name);
signal purchase_fail;

const _singleton_name = "InAppStore";

var _logger = Log.get_logger("ios_iap.gd");
var _in_app_store;

var _delay_time = 0.5;
var _try = 0;

const _retry_count = 1000;

func _init() -> void:
	if Engine.has_singleton(_singleton_name):
		_in_app_store = Engine.get_singleton(_singleton_name);
		_logger.info("Initialized iOS IAP");
	else:
		_logger.info("iOS IAP not supported");

func is_supported():
	return _in_app_store != null;

func purchase(id):
	var result = _in_app_store.purchase( { "product_id": id } );
	_logger.info("got result %s" %result);
	if result == OK:
		_logger.info("Purchase success");
		_check_response();
	else:
		_logger.info("Error while purchasing");

func _check_response():
	_logger.info("Checking response");
	
	while true:
		if _in_app_store.get_pending_event_count() == 0:
			continue;
			
		var event = _in_app_store.pop_pending_event();
			
		_logger.info("Response %s" %event);

		if event.type != "purchase":
			continue;

		if event.result == "ok":
			emit_signal("purchase_success", event.product_id);
			return;
		else:
			emit_signal("purchase_fail");
			return;

func get_info():
	var result =_in_app_store.request_product_info( { "product_ids": ["com.domas.snek.snek_maze_unlock"] } );
	_logger.info("Info %s" %result);