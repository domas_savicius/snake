extends Node

onready var _bonus_pickup_sound: AudioStreamPlayer2D = $bonus_pickup;
onready var _pickup_sound: AudioStreamPlayer2D = $food_pickup;

var _sound_enabled;

func _ready() -> void:
	Events.connect_food_consumed(self, "_on_food_consumed");
	_init_settings();

func _init_settings():
	_sound_enabled = Settings.get_sound();

func _on_food_consumed(food):
	if _sound_enabled:
		if !food.is_normal():
			_bonus_pickup_sound.play();
		else:
			_pickup_sound.play();
