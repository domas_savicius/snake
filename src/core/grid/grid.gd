extends Node2D

var speed: float = 9;
var level;

var _hit_delay;

var _logger = Log.get_logger("grid.gd");

onready var _level_tilemap: TileMap = $level_tiles;
onready var _obstacle_tilemap: TileMap = $obstacle_tiles;
onready var _move_timer: Timer = $move_timer;

onready var _wall_hit_sound = $wall_hit_sound;

onready var _snake_tile = preload("res://core/snake/snake_tile.tscn");
onready var _snake_tile_head = preload("res://core/snake/snake_tile_head.tscn");

export (Vector2) var starting_point = Vector2(GridConstants.start_x, GridConstants.start_y);

var _snake = [];

var _food_tiles = {};

onready var _direction_input: DirectionInput = $direction_input;

func _ready():
	_init_speed();
	_init_hit_delay();
	_init_events();
	_init_snake();
	_init_timer();
	GameState.reset();

func _init_hit_delay():
	_hit_delay = HitDelay.new(speed);

func _init_speed():
	var game_options = GameOptions.new();
	speed = game_options.get_speed();
	level = game_options.get_maze();
	_logger.info("Init speed %s" %speed);

func _init_events():
	var level_options = LevelOptions.new(speed, level, _obstacle_tilemap.get_used_cells());
	Events.connect_create_food(self, "_on_create_food");
	Events.emit_game_started(_snake, level_options);

func _init_timer():
	var movement_wait_time = MovementWaitTime.new();
	var wait_time = movement_wait_time.get_wait_time(speed);
	_logger.info("Wait time %s" %wait_time);
	_move_timer.wait_time = wait_time;
	_move_timer.start();

func _init_snake():
	for i in range(GridConstants.start_length):
		var position = Vector2(starting_point.x + i, starting_point.y);
		var position_in_grid = position;
		var tile;
		if i == GridConstants.start_length - 1:
			tile = _create_snake_head_tile(position_in_grid);
		else:
			tile = _create_snake_body_tile(position_in_grid);

		_snake.append(tile);

func _create_snake_body_tile(position_in_grid):
	var tile = _snake_tile.instance();
	return _create_snake_tile(tile, position_in_grid);

func _create_snake_head_tile(position_in_grid):
	var tile = _snake_tile_head.instance();
	return _create_snake_tile(tile, position_in_grid);

func _create_snake_tile(tile ,position_in_grid):
	var world_position = _level_tilemap.map_to_world(position_in_grid);

	_level_tilemap.add_child(tile);

	tile.set_position(world_position);
	tile.set_position_in_grid(position_in_grid);

	return tile;

func _process(delta):
	_direction_input.process_direction();

func _on_move_timer_timeout():
	if GameState.is_game_over():
		return;
	
	var current_direction = _direction_input.get_current_direction();
	perform_move(current_direction);

func perform_move(move_direction: Vector2):
	var consumed = false;
	var tail_position;

	GameState.set_moving();

	for i in range(_snake.size() - 1, -1, -1):
		var tile = _snake[i];
		var is_head = tile.is_head;
		var tile_position = tile.get_position_in_grid();

		var new_position: Vector2;
		if is_head:
			new_position = tile_position + move_direction;
			tile.set_direction(move_direction);
			if perform_delay_before_hit(new_position):
				return;
		else:
			var next_tile = _snake[i + 1];
			new_position = next_tile.get_previous_position_in_grid();
			tail_position = tile_position;

		if is_head && check_obstacle(new_position):
			on_obstacle_hit(position);
			return;

		if is_out_of_bounds(new_position):
			new_position = get_adjusted_tile_position(new_position);

		set_snake_tile_position(tile, new_position);

		if is_head:
			consumed = check_food(new_position);

	if consumed:
		var new_tile = _create_snake_body_tile(tail_position);
		_snake.push_front(new_tile);

func perform_delay_before_hit(position):
	if !check_obstacle(position):
		_hit_delay.reset();
		return false;

	if _hit_delay.is_in_delay():
		return _hit_delay.tick();
	else:
		_hit_delay.start();
		return true;

func on_obstacle_hit(position):
	_logger.info("Hit obstacle at %s" %position);
	_wall_hit_sound.play();
	GameState.set_game_over();
	Events.emit_game_over();

func is_out_of_bounds(new_position):
	var x = new_position.x;
	var y = new_position.y;
	if x >= 0 && y >= 0 && x < GridConstants.width && y < GridConstants.height:
		return false;

	_logger.info("Out of bounds at %s" %new_position);
	return true;

func get_adjusted_tile_position(tile_position):
	if tile_position.x >= GridConstants.width:
		return Vector2(0, tile_position.y);
	if tile_position.y >= GridConstants.height:
		return Vector2(tile_position.x, 0);
	if tile_position.x < 0:
		return Vector2(GridConstants.width - 1, tile_position.y);
	if tile_position.y < 0:
		return Vector2(tile_position.x, GridConstants. height - 1);

	assert(false);

func set_snake_tile_position(tile, position):
	var world_position = _level_tilemap.map_to_world(position);
	tile.set_position_in_grid(position);
	tile.set_position(world_position);

func check_food(position: Vector2):
	if _food_tiles.has(position):
		var food = _food_tiles[position];
		if _is_valid_food_instance(food):
			food.consume();
			_food_tiles.erase(position);
			_logger.info("Consumed food %s at %s" %[food, position]);
			return true;

	return false;

func _is_valid_food_instance(food: Food) -> bool:
	return food && is_instance_valid(food);

func check_obstacle(position: Vector2):
	if is_hitting_itself(position):
		return true;

	if is_hitting_wall(position):
		return true;

	return false;

func is_hitting_itself(position):
	for tile in _snake:
		if !tile.is_head && tile.get_position_in_grid() == position:
			return true;
	return false;

func is_hitting_wall(position):
	var index = _obstacle_tilemap.get_cellv(position);

	if index == TileMap.INVALID_CELL:
		return false;

	_logger.info("Hit obstacle at %s" %position);
	return true;

# -> Events ========================================================================================================
func _on_create_food(food, position):
	var world_position = _level_tilemap.map_to_world(position);
	_level_tilemap.add_child(food);
	food.set_position(world_position);

	_food_tiles[position] = food;