extends Node

class_name HitDelay

var _logger = Log.get_logger("hit_delay.gd");

var _default_ticks_to_delay;

var _ticks_to_delay = 0;
var _is_in_delay = false;

func _init(speed) -> void:
	var movement_speed = MovementWaitTime.new();
	var time_to_delay = movement_speed.get_wait_time(1);
	
	_default_ticks_to_delay = int(round(time_to_delay / movement_speed.get_wait_time(speed)));
	_logger.info("Default ticks to wait %s" %_default_ticks_to_delay);

func start():
	_logger.info("Begin delay before hit");
	_ticks_to_delay = _default_ticks_to_delay;
	_is_in_delay = true;

func is_in_delay():
	return _is_in_delay;

func tick():
	if _ticks_to_delay <= 0:
		return false;

	_ticks_to_delay -= 1;
	_logger.info("Delayed, ticks left %s" %_ticks_to_delay);
	
	return true;

func reset():
	if _is_in_delay:
		_ticks_to_delay = 0;
		_is_in_delay = false;
		_logger.info("Reset hit delay");