extends Node

enum State { MOVING, CHANGED_DIRECTION, GAME_OVER, PAUSED, RESUMED }

const _default_state = State.MOVING;

var _logger = Log.get_logger("game_state.gd");
var _state = _default_state;

func reset():
	_state = _default_state;

func _set_state(state):
	if _state == state:
		return;

	if _is_final_state():
		return;

	if is_paused() && !_is_resuming_from_pause(state):
		# Allow to change from PAUSED to RESUME only
		return;

	_state = state;
	_logger.info("Changed state to %s" %_state);

func set_changed_direction():
	_set_state(State.CHANGED_DIRECTION);

func set_moving():
	_set_state(State.MOVING);

func set_paused():
	_set_state(State.PAUSED);

func set_resumed():
	_set_state(State.RESUMED);

func is_changed_direction():
	return _state == State.CHANGED_DIRECTION;

func set_game_over():
	_set_state(State.GAME_OVER);

func is_game_over():
	return _state == State.GAME_OVER;

func is_paused():
	return _state == State.PAUSED;

func is_resumed():
	return _state == State.RESUMED;

func _is_final_state():
	match _state:
		State.GAME_OVER:
			return true;

	return false;

func _is_resuming_from_pause(new_state):
	if new_state == State.GAME_OVER:
		return true;

	if new_state != State.RESUMED:
		return true;

	return false;