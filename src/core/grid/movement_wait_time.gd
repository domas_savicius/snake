extends Node

class_name MovementWaitTime

func get_wait_time(speed: int):
	return 1.0 * (GameOptions.HIGHEST_SPEED - speed + 1) / 50;