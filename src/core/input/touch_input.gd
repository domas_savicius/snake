extends Node2D

class_name TouchInput

var _logger = Log.get_logger("touch_input.gd");

var _screen_size;
onready var _container = get_node("/root/game/game/Container");
onready var _bottom = get_node("/root/game/game/bottom");

const _ui_touch = "ui_touch";
const _bottom_ui_x = 80;

enum ScreenSquare { UPPER_LEFT, UPPER_RIGHT, LOWER_LEFT, LOWER_RIGHT };

func _ready() -> void:
	get_tree().get_root().connect("size_changed", self, "_on_resize");
	_on_resize();

func get_direction(current_direction: Vector2) -> Vector2:
	if Input.is_action_just_pressed(_ui_touch):
		return _get_position_from_touch(current_direction);
	
	if Input.is_action_just_pressed("ui_up"):
		return Vector2.UP;
	if Input.is_action_just_pressed("ui_down"):
		return Vector2.DOWN;
	if Input.is_action_just_pressed("ui_left"):
		return Vector2.LEFT;
	if Input.is_action_just_pressed("ui_right"):
		return Vector2.RIGHT;

	return Vector2.ZERO;

func _is_touching_bottom_ui(touch_position: Vector2) -> bool:
	if touch_position.y >= _bottom.rect_global_position.y:
		_logger.info("Touching the bottom UI area");
		return true;

	return false;
	
func _get_position_from_touch(current_direction: Vector2) -> Vector2:
	var touch_position = get_global_mouse_position();
	var new_direction;
	_logger.info("Touch at %s" %touch_position);
	if _is_touching_bottom_ui(touch_position):
		return Vector2.ZERO;
	
	if _is_going_up_or_down(current_direction):
		new_direction = _get_horizontal_direction(touch_position);
	elif _is_going_left_or_right(current_direction):
		new_direction = _get_vertical_direction(touch_position);

	_logger.info("New direction %s" %new_direction);
	return new_direction;

func _is_going_left_or_right(direction: Vector2) -> bool:
	return direction == Vector2.LEFT || direction == Vector2.RIGHT;

func _is_going_up_or_down(direction: Vector2) -> bool:
	return direction == Vector2.UP || direction == Vector2.DOWN;

func _get_horizontal_direction(touch_position: Vector2) -> Vector2:
	var square = _get_screen_square(touch_position);
	if square == ScreenSquare.LOWER_LEFT || square == ScreenSquare.UPPER_LEFT:
		return Vector2.LEFT;

	if square == ScreenSquare.LOWER_RIGHT || square == ScreenSquare.UPPER_RIGHT:
		return Vector2.RIGHT;

	return Vector2.ZERO;

func _get_vertical_direction(touch_position: Vector2) -> Vector2:
	var square = _get_screen_square(touch_position);
	if square == ScreenSquare.UPPER_LEFT || square == ScreenSquare.UPPER_RIGHT:
		return Vector2.UP;

	if square == ScreenSquare.LOWER_LEFT || square == ScreenSquare.LOWER_RIGHT:
		return Vector2.DOWN;

	return Vector2.ZERO;

func _get_screen_square(touch_position):
	var grid_y = touch_position.y - _container.rect_global_position.y;

	var half_screen_x = _screen_size.x / 2;
	var half_grid_y = _container.rect_size.y / 2;

	var is_left = touch_position.x < half_screen_x;
	var is_lower = grid_y > half_grid_y;

	var square;

	if is_left:
		if is_lower:
			square = ScreenSquare.LOWER_LEFT;
		else:
			square = ScreenSquare.UPPER_LEFT;
	else:
		if is_lower:
			square = ScreenSquare.LOWER_RIGHT;
		else:
			square = ScreenSquare.UPPER_RIGHT;

	_logger.info("Touched square %s" %square);
	return square;

func _on_resize():
	_screen_size = get_viewport().get_visible_rect().size;