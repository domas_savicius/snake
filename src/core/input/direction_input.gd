extends Node2D

class_name DirectionInput

var _logger = Log.get_logger("direction_input.gd");

onready var _touch_input = $touch_input;
var _direction: Vector2 = Vector2.RIGHT;

func process_direction():
	var new_direction = _get_direction();
	if new_direction == Vector2.ZERO:
		return;

	_direction = new_direction;
	GameState.set_changed_direction();

	_logger.info("Changed direction to %s" %new_direction);

func _get_direction() -> Vector2:
	var new_direction = _get_new_direction();
	
	if !_is_valid_direction(new_direction):
		return Vector2.ZERO;

	return new_direction;

func _is_valid_direction(new_direction):
	return new_direction != _direction * -1 && new_direction != Vector2.ZERO;
	
func _get_new_direction() -> Vector2:
	if !_can_get_new_direction():
		return Vector2.ZERO;
		
	return _touch_input.get_direction(_direction);

func get_current_direction():
	return _direction;

func _can_get_new_direction():
	return !GameState.is_changed_direction() && !GameState.is_paused();