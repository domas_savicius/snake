extends Node

class_name LinkedCollectionWrapper

var _inner;
var _index = 0;

func _init(array: Array):
	_inner = array;
	
func get_current():
	return _inner[_index];

func back():
	var new_index = _index - 1;
	if new_index < 0:
		new_index = _inner.size() - 1;

	_index = new_index;

func forward():
	var new_index = _index + 1;
	if new_index >= _inner.size():
		new_index = 0;

	_index = new_index;

func set_selected(value):
	for i in _inner.size():
		var current = get_current();
		if current == value:
			_index = i;
			return;
			
		forward();
