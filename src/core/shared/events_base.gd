extends Node

class_name EventsBase

var _logger = Log.get_logger("events");

func _connect(signal_name, target, func_name):
	connect(signal_name, target, func_name);
	_logger.info("Connected %s to %s" %[signal_name, func_name]);