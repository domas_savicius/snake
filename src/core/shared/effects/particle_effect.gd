extends Container

onready var _particles: Particles2D = $effect/particles;

func _ready() -> void:
	_particles.set_emitting(true);
	_particles.set_one_shot(true);

func _on_timer_timeout() -> void:
	queue_free();
