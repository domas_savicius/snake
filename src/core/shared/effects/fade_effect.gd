extends Node2D

onready var _tween: Tween = $tween;
onready var _texture = $texture;

const _duration = 0.3;

var _logger = Log.get_logger("fade_effect.gd");

const _transparent = Color(1, 1, 1, 0);
const _black = Color(1, 1, 1, 1);

func fade_in():
	yield(_start_tween(_transparent, _black), "completed");
	
func fade_out():
	yield(_start_tween(_black, _transparent), "completed");
	
func _start_tween(from, to):
	_tween.interpolate_property(_texture, "modulate", from, to, _duration, Tween.TRANS_LINEAR, Tween.EASE_IN);
	_tween.start();
	yield(_tween, "tween_completed");