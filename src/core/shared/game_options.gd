extends Node

class_name GameOptions

const HIGHEST_SPEED = 15;

var SPEED_VALUES = []

const MAZE_NONE = "NONE";
const MAZE_VALUES = [ MAZE_NONE, "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11" ]

const _config_section = "options";
const _iap_section = "iap";
const _iap_key = "11172740-509f-4572-a133-520c651bf9bc";

var _data = GameData.new();

func _init():
	for i in range(HIGHEST_SPEED):
		SPEED_VALUES.append(i + 1);

func get_speed():
	var speed = _data.get_value("speed", _config_section, 9);
	return speed;

func set_speed(value):
	_data.set_value("speed", value, _config_section);

func get_maze():
	var maze = _data.get_value("maze", _config_section);
	if not maze:
		return MAZE_NONE;

	return maze;

func set_maze(value):
	_data.set_value("maze", value, _config_section);

func mazes_unlocked() -> bool:
	var is_unlocked = _data.get_value(_iap_key, _iap_section, false);
	return is_unlocked;

func unlock_mazes() -> void:
	_data.set_value(_iap_key, true, _iap_section);

func is_maze_locked(maze: String) -> bool:
	if maze == MAZE_NONE:
		return false;

	return !mazes_unlocked();