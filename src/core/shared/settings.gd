extends Node

var _data;
var _section = "settings";

func _ready() -> void:
	_data = GameData.new();

func set_vibration(value: bool):
	_data.set_value("vibration", value, _section);

func get_vibration():
	return _data.get_value("vibration", _section, true);

func set_sound(value: bool):
	_data.set_value("sound", value, _section);

func get_sound():
	return _data.get_value("sound", _section, true);