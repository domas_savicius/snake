extends Object

class_name KeyValuePair

var key;
var value;

func _init(_key, _value) -> void:
	self.key = _key;
	self.value = _value;

static func sort_desc(a: KeyValuePair, b: KeyValuePair):
	if a.value > b.value:
		return true
	return false