extends Node

var _game_menu = preload("res://core/ui/main_menu.tscn");
var _high_scores = preload("res://core/ui/high_score/high_scores.tscn");
var _game = preload("res://core/game.tscn");

var _fade_effect = preload("res://core/shared/effects/fade_effect.tscn");

var _logger = Log.get_logger("scene_manager.gd");

func pause():
	get_tree().paused = true;
	GameState.set_paused();

func resume():
	get_tree().paused = false;
	GameState.set_resumed();

func reload_current_scene():
	_logger.info("Reloading scene");
	var error_code = get_tree().reload_current_scene();
	resume();

	if error_code != OK:
		_logger.info("Error reloading scene, code %s" %error_code);

func change_scene_to_game_menu():
	_change_scene(_game_menu);

func change_scene_to_game():
	_change_scene(_game);

func change_scene_to_high_scores():
	_change_scene(_high_scores);

func change_to_level(level):
	var err = get_tree().change_scene("res://core/level/level_scenes/level_" + level.to_lower() + ".tscn");
	if err != OK:
		_logger.info("Error %s changing to level scene %s" %[err, level]);

func _change_scene(scene: PackedScene):
	_logger.info("Changing scene to %s" %scene);

	var root = get_tree().root;
	var effect = _fade_effect.instance();
	root.add_child(effect);

	yield(effect.fade_in(), "completed");
	get_tree().change_scene_to(scene);

	_logger.info("Scene changed");

	yield(effect.fade_out(), "completed");
	effect.queue_free();