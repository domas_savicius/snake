extends EventsBase

# -> Show pause menu
signal show_popup_pause();
func emit_show_popup_pause(): emit_signal("show_popup_pause");
func connect_show_popup_pause(target, func_name): _connect("show_popup_pause", target, func_name);

# -> Show game over popup
signal show_poup_game_over(score);
func emit_show_poup_game_over(score): emit_signal("show_poup_game_over", score);
func connect_show_poup_game_over(target, func_name): _connect("show_poup_game_over", target, func_name);

# -> Show game start options popup
signal show_start_game_popup();
func emit_show_start_game_popup(): emit_signal("show_start_game_popup");
func connect_show_start_game_popup(target, func_name): _connect("show_start_game_popup", target, func_name);

# -> Show IAP popup
signal show_iap_popup();
func emit_show_iap_popup(): emit_signal("show_iap_popup");
func connect_show_iap_popup(target, func_name): _connect("show_iap_popup", target, func_name);

# -> IAP button pressed
signal iap_button_pressed(id);
func emit_iap_button_pressed(id): emit_signal("iap_button_pressed", id);
func connect_iap_button_pressed(target, func_name): _connect("iap_button_pressed", target, func_name);