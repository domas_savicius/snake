extends Node

class_name GameData

var _logger = Log.get_logger("game_data.gd");

var _path = "user://data.ini";

func _get_file() -> ConfigFile:
	var file = ConfigFile.new();
	var err = file.load(_path);
	if err == OK:
		return file;

	file = _try_create();
	if file:
		_logger.info("Created empty");
		return file;

	_logger.error("Could not load data.ini, err %s" %err);
	return null;

func _try_create():
	var file = ConfigFile.new();
	file.save(_path);
	return file;

func _get_key(level, speed):
	return "level_%s_speed_%s" %[level, speed];

func get_value(key, section, default = null):
	var file = _get_file();
	if !file:
		return null;

	return file.get_value(section, key, default);

func get_all_values_in_section(section) -> Array:
	var file = _get_file();
	if !file:
		return [];

	var results = [];
	var keys = file.get_section_keys(section);
	for key in keys:
		var value = get_value(key, section);
		var key_value = KeyValuePair.new(key, value);
		results.append(key_value);

	return results;

func set_value(key, value, section) -> void:
	var file = _get_file();
	if !file:
		return;

	file.set_value(section, key, value);
	file.save(_path);
	_logger.info("Set %s, value %s. Section %s" %[key, value, section]);
