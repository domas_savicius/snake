extends Node

var _notification = preload("res://core/shared/notification.tscn");

func show(text: String):
	var notification = _notification.instance();
	notification.set_text(text);
	
	var root = get_tree().root;
	root.add_child(notification);