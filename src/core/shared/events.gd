extends EventsBase

## Game events ---------------------------------------------------------------------------------------

# -> Play pressed
signal pressed_play(speed);
func emit_pressed_play(speed): emit_signal("pressed_play", speed);
func connect_pressed_play(target, func_name): _connect("pressed_play", target, func_name);

# -> Game start
signal game_started(snake_array, options);
func emit_game_started(snake_array, options: LevelOptions): emit_signal("game_started", snake_array, options);
func connect_game_started(target, func_name): _connect("game_started", target, func_name);

# -> Score changed
signal score_changed(score);
func emit_score_changed(score): emit_signal("score_changed", score);
func connect_score_changed(target, func_name): _connect("score_changed", target, func_name);

# -> Game over
signal game_over();
func emit_game_over(): emit_signal("game_over");
func connect_game_over(target, func_name): _connect("game_over", target, func_name);

# -> Load high score
signal load_high_score(high_score);
func emit_load_high_score(high_score): emit_signal("load_high_score", high_score);
func connect_load_high_score(target, func_name): _connect("load_high_score", target, func_name);

# -> New high score
signal new_high_score_reached(score);
func emit_new_high_score_reached(score): emit_signal("new_high_score_reached", score);
func connect_new_high_score_reached(target, func_name): _connect("new_high_score_reached", target, func_name);

##Food events ---------------------------------------------------------------------------------------

# -> Create food
signal create_food(food, position);
func emit_create_food(food, position): emit_signal("create_food", food, position);
func connect_create_food(target, func_name): _connect("create_food", target, func_name);

# -> Food eaten
signal food_consumed(food);
func emit_food_consumed(food): emit_signal("food_consumed", food);
func connect_food_consumed(target, func_name): _connect("food_consumed", target, func_name);

# -> Bonus food time (second) passes
signal bonus_food_tick(time_left);
func emit_bonus_food_tick(time_left): emit_signal("bonus_food_tick", time_left);
func connect_bonus_food_tick(target, func_name): _connect("bonus_food_tick", target, func_name);

# -> Bonus food time ended
signal bonus_food_ended;
func emit_bonus_food_ended(): emit_signal("bonus_food_ended");
func connect_bonus_food_ended(target, func_name): _connect("bonus_food_ended", target, func_name);

# -> Bonus food spawned
signal bonus_food_spawned(food);
func emit_bonus_food_spawned(food): emit_signal("bonus_food_spawned", food);
func connect_bonus_food_spawned(target, func_name): _connect("bonus_food_spawned", target, func_name);

##IAP events---------------------------------------------------------------------------------------
# -> Purchase success
signal purchase_success(id);
func emit_purchase_success(id): emit_signal("purchase_success", id);
func connect_purchase_success(target, func_name): _connect("purchase_success", target, func_name);

# -> Purchase fail
signal purchase_fail(id);
func emit_purchase_fail(id): emit_signal("purchase_fail", id);
func connect_purchase_fail(target, func_name): _connect("purchase_fail", target, func_name);

#-> Purchase cancel
signal purchase_cancel(id);
func emit_purchase_cancel(id): emit_signal("purchase_cancel", id);
func connect_purchase_cancel(target, func_name): _connect("purchase_cancel", target, func_name);