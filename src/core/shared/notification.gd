extends CanvasLayer

onready var _container = $Container;
onready var _label = $Container/center/content;
onready var _tween = $Container/disappear_tween;
onready var _timer = $Container/timer;

var _text;
var _disappear_timer = 2.0;

func _ready() -> void:
	_label.text = _text;

	yield(get_tree().create_timer(1.0), "timeout");
	_tween.interpolate_property(_container, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), _disappear_timer, Tween.TRANS_LINEAR, Tween.EASE_IN);
	_tween.start();
	_init_timer();

func set_text(text: String):
	_text = text;

func _init_timer():
	_timer.start(_disappear_timer);

func _on_timer_timeout() -> void:
	queue_free();