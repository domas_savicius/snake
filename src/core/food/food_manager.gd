extends Node

var _logger = Log.get_logger("food_manager.gd");

onready var _food = preload("res://core/food/food.tscn");
onready var _bonus_food = preload("res://core/food/bonus_food.tscn");

var _snake;
var _obstacle_positions;
var _rng = RandomNumberGenerator.new();

var _number_of_consumed_food = 0;

var _vibration_enabled;

func _ready():
	_rng.randomize();
	_init_events();
	_init_settings();

func _init_settings():
	_vibration_enabled = Settings.get_vibration();

func _init_events():
	Events.connect_game_started(self, "_on_game_started");
	Events.connect_food_consumed(self, "_on_food_consumed");

func _on_game_started(snake_array, _options):
	_snake = snake_array;
	_obstacle_positions = _options.obstacle_positions;
	_create_simple_food();

func _on_food_consumed(food):
	if _vibration_enabled:
		Input.vibrate_handheld();

	if !food.is_normal():
		return;
		
	_number_of_consumed_food += 1;
	_create_simple_food();

	if _should_create_bonus_food():
		_create_bonus_food();

func _create_food_instance(food_instance):
	var position = _get_random_position();
	while !_is_valid_position(position):
		position = _get_random_position();
		#TODO when full screen?

	_logger.info("Creating food at %s" %position);
	Events.emit_create_food(food_instance, position);

func _create_simple_food():
	_create_food_instance(_food.instance());

func _create_bonus_food():
	_create_food_instance(_bonus_food.instance());

func _is_valid_position(position):
	for snake_tile in _snake:
		if snake_tile.get_position_in_grid() == position:
			return false;

	for obstacle in _obstacle_positions:
		if obstacle == position:
			return false; 
		
	return true;

func _get_random_position():
	var rand_x = _rng.randi() % GridConstants.width;
	var rand_y = _rng.randi() % GridConstants.height;

	return Vector2(rand_x, rand_y);

func _should_create_bonus_food():
	return _number_of_consumed_food % 5 == 0;