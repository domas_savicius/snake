extends Node2D

class_name Food

var _tween_values = [0.3, 0.7];
var value = 1;

onready var _light = $Sprite/light;
onready var _tween = $Sprite/light/tween;

var _effect = preload("res://core/shared/effects/consume_food_particle_effect.tscn");

func _ready() -> void:
	_tween.connect("tween_completed", self, "_on_tween_completed");
	_start_tween();

func _start_tween():
	_tween.interpolate_property(_light, "energy", _tween_values[0], _tween_values[1], 1, Tween.TRANS_LINEAR, Tween.EASE_OUT);
	_tween.start();

func _on_tween_completed(object, key):
	_tween_values.invert();
	_start_tween();

func consume():
	_create_consume_effect();
	Events.emit_food_consumed(self);
	queue_free();

func get_score(speed):
	return value * speed;

func is_normal():
	return true;

func _create_consume_effect():
	_spawn_effect(_effect);

func _spawn_effect(effect):
	var effect_instance = effect.instance();
	effect_instance.set_position(position);
	get_parent().add_child(effect_instance);
