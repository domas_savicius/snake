extends Food

onready var _timer = $timer;

var _time_to_live;

var _time_ended_effect = preload("res://core/shared/effects/bonus_food_ended_particle_effect.tscn");

func _ready() -> void:
	_time_to_live = 5;
	Events.emit_bonus_food_spawned(self);
	Events.emit_bonus_food_tick(_time_to_live);
	_timer.start();

func get_score(speed):
	return 5 * speed;

func _on_timer_timeout() -> void:
	_time_to_live -= 1;
	Events.emit_bonus_food_tick(_time_to_live);
	
	if _time_to_live <= 0:
		_destroy(false);

func _destroy(consumed: bool):
	if not consumed:
		._spawn_effect(_time_ended_effect);

	Events.emit_bonus_food_ended();
	queue_free();

func is_normal():
	return false;

func consume():
	.consume();
	_destroy(true);