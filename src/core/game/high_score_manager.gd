extends Node

class_name HighScoreManager

var _logger = Log.get_logger("high_score_manager.gd");

var _game_data = GameData.new();

var _section = "highscores";

var _level;
var _speed;
var _high_score;
var _current_score;

var _new_high_score_this_session = false;

func _ready() -> void:
	Events.connect_game_started(self, "_on_game_started");
	Events.connect_score_changed(self, "_on_score_changed");
	_current_score = 0;

func _on_score_changed(score):
	_current_score = score;
	if _current_score > _high_score:
		set_high_score(_level, _current_score);
		_emit_new_high_score(score);

func _on_game_started(_snake_array, options:LevelOptions):
	_level = options.level;
	_speed = options.speed;

	_high_score = get_high_score(_level);
	Events.emit_load_high_score(_high_score);

func _get_key(level):
	return "level_%s" %level;

func get_high_score(level) -> int:
	var key = _get_key(level);
	return _game_data.get_value(key, _section, 0);

func get_absolute_high_score() -> int:
	var high_scores = get_all_high_scores();
	var highest = high_scores.front();
	
	if !highest:
		return 0;
	
	return highest.value;

func get_all_high_scores()-> Array:
	var high_scores = _game_data.get_all_values_in_section(_section);
	high_scores.sort_custom(KeyValuePair, "sort_desc");
	return high_scores;

func set_high_score(level, score) -> void:
	var key = _get_key(level);
	_game_data.set_value(key, score, _section);
	_logger.info("Set high score %s" %score);

func _emit_new_high_score(score: int):
	if !_new_high_score_this_session:
		Events.emit_new_high_score_reached(score);
		_new_high_score_this_session = true;
		_logger.info("New high score reached this session, %s" %score);