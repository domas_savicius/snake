extends Node

class_name GameManager

var _logger = Log.get_logger("game_manager.gd");

var _options;
var _score = 0;

var _snake;

func _ready():
	Events.connect_game_started(self, "_on_game_started");
	Events.connect_game_over(self, "_on_game_over");
	Events.connect_food_consumed(self, "_on_food_consumed");

func _on_game_started(snake_array, options):
	_options = options;
	_snake = snake_array;
	_logger.info("Initialized");

func _on_food_consumed(food):
	var score_increment = food.get_score(_options.speed);
	_score += score_increment;
	Events.emit_score_changed(_score);
	_logger.info("Incrementing the score by %s, new score %s" %[score_increment, _score]);

func _on_game_over():
	Input.vibrate_handheld();
	GameState.set_game_over();
	EventsUI.emit_show_poup_game_over(_score);