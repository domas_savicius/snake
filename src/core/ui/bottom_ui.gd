extends Control

func _ready() -> void:
	pass

func _on_pause_button_pressed() -> void:
	EventsUI.emit_show_popup_pause();