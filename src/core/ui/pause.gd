extends BasePopup

func _ready() -> void:
	_name = "PAUSE";
	set_title(_name);
	EventsUI.connect_show_popup_pause(self, "_on_show_popup");

func _on_show_popup():
	show_popup();

func _on_ok_pressed() -> void:
	hide_popup();

func _on_cancel_pressed() -> void:
	SceneManager.resume();
	SceneManager.change_scene_to_game_menu();