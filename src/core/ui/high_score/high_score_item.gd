extends Control

onready var _level_label = $center/HBoxContainer/level;
onready var _score_label = $center/HBoxContainer/score;

var _level;
var _score;
var _init = false;

func _ready() -> void:
	if !_init:
		assert(false);

	_level_label.text = _level;
	_score_label.text = _score;

func set_values(level, score):
	_level = level;
	_score = String(score);
	_init = true;
