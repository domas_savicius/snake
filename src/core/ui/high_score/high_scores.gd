extends Node

onready var _maze_scores_container = $canvas/content/VBoxContainer/maze_scores;
onready var _top_score_label = $canvas/content/VBoxContainer/score_label;

const _max_score_count = 10;

var _high_score_list_item = preload("res://core/ui/high_score/high_score_item.tscn");

var _game_data = GameData.new();

func _ready() -> void:
	var high_score_manager = HighScoreManager.new();

	var scores = high_score_manager.get_all_high_scores();
	var highest = high_score_manager.get_absolute_high_score();

	_top_score_label.text = String(highest);

	var count = 0;
	for score in scores:
		var item_instance = _high_score_list_item.instance();
		item_instance.set_values(score.key.capitalize().replace("Level", "Maze"), score.value);

		_maze_scores_container.add_child(item_instance);

		count += 1;
		if count >= _max_score_count:
			return;

func _on_back_pressed() -> void:
	SceneManager.change_scene_to_game_menu();