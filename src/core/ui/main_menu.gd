extends Node

onready var _sound_button: TextureButton = $canvas/content/VBoxContainer/settings_buttons/HBoxContainer/sound_button;
onready var _vibration_button: TextureButton = $canvas/content/VBoxContainer/settings_buttons/HBoxContainer/vibration_button;
onready var _iap_button: TextureButton = $canvas/content/VBoxContainer/settings_buttons/HBoxContainer/iap;

func _ready() -> void:
	_sound_button.pressed  = Settings.get_sound();
	_vibration_button.pressed = Settings.get_vibration();
	if !InAppPurchaceManager.is_supported():
		_iap_button.visible = false;

func _on_play_button_pressed() -> void:
	EventsUI.emit_show_start_game_popup();

func _on_sound_button_toggled(button_pressed: bool) -> void:
	Settings.set_sound(button_pressed);

func _on_vibration_button_toggled(button_pressed: bool) -> void:
	Settings.set_vibration(button_pressed);

func _on_iap_pressed() -> void:
	InAppPurchaceManager.purchase_levels();

func _on_high_scores_button_pressed() -> void:
	SceneManager.change_scene_to_high_scores();