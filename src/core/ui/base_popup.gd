extends Popup

class_name BasePopup

onready var _title_label = $popup_content/VBoxContainer/title/Label;

var _logger = Log.get_logger("base_popup.gd");
var _name;

func show_popup(pause: bool = true):
	_validate();
	show_modal();
	if pause:
		SceneManager.pause();
	_logger.info("Opened a popup: %s" %name);

func hide_popup():
	hide();
	SceneManager.resume();

func _validate():
	if(!_name):
		assert(false);

func set_title(title: String):
	_title_label.text = title;

func _on_ok_pressed() -> void:
	pass;

func _on_cancel_pressed() -> void:
	pass;