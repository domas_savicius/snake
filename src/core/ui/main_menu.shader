shader_type canvas_item;

void fragment()
{
    COLOR = texture(TEXTURE, UV);
	
	COLOR.g = abs(sin(TIME + UV.r));
	COLOR.b = abs(sin(TIME + UV.g + UV.r));
}