extends BasePopup

var _game_options = GameOptions.new();

var _speed;
var _maze;

onready var _speed_label = $popup_content/VBoxContainer/speed/CenterContainer/speed_content/CenterContainer2/speed_value;
onready var _maze_label = $popup_content/VBoxContainer/maze/CenterContainer/maze_content/CenterContainer2/maze_value;

onready var _start_button_container = $popup_content/VBoxContainer/button/start;
onready var _purchase_button_container = $popup_content/VBoxContainer/button/locked;

func _ready() -> void:
	_name = "Start game options";
	EventsUI.connect_show_start_game_popup(self, "_on_show_start_game_popup");
	Events.connect_purchase_success(self, "_on_purchase_success");
	_speed = LinkedCollectionWrapper.new(_game_options.SPEED_VALUES);
	_maze = LinkedCollectionWrapper.new(_game_options.MAZE_VALUES);
	_load();

func _on_purchase_success(_item):
	_game_options.unlock_mazes();
	_logger.info("Mazes unlocked");

func _on_show_start_game_popup():
	show_popup(false);
	_refresh_maze();

func _load() -> void:
	_speed.set_selected(_game_options.get_speed());
	_maze.set_selected(_game_options.get_maze());

	_refresh_maze();
	_refresh_speed();

func _on_speed_arrow_left_pressed() -> void:
	 _speed.back();
	 _refresh_speed();

func _on_speed_arrow_right_pressed() -> void:
	_speed.forward();
	_refresh_speed();

func _on_maze_arrow_left_pressed() -> void:
	_maze.back();
	_refresh_maze();

func _on_maze_arrow_right_pressed() -> void:
	_maze.forward();
	_refresh_maze();

func _on_start_button_pressed() -> void:
	hide_popup();
	SceneManager.change_to_level(_maze.get_current());

func _refresh_speed():
	var new_val = _speed.get_current();
	_speed_label.text = String(new_val);
	_game_options.set_speed(new_val);

func _refresh_maze():
	var new_val = _maze.get_current();
	_maze_label.text = new_val;

	if _game_options.is_maze_locked(new_val):
		hide_start();
	else:
		show_start();
		_game_options.set_maze(new_val);

func _on_purchase_button_pressed() -> void:
	hide_popup();
	InAppPurchaceManager.purchase_levels();

func hide_start() -> void:
	_start_button_container.visible = false;
	_purchase_button_container.visible = true;

func show_start()-> void:
	_start_button_container.visible = true;
	_purchase_button_container.visible = false;