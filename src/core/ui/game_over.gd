extends BasePopup

func _ready() -> void:
	_name = "GAME OVER";
	set_title(_name);
	EventsUI.connect_show_poup_game_over(self, "_on_show_game_over");

func _on_show_game_over(_score):
	show_popup();

func _on_ok_pressed() -> void:
	SceneManager.reload_current_scene();

func _on_cancel_pressed() -> void:
	SceneManager.resume();
	SceneManager.change_scene_to_game_menu();