extends TextureButton

export (String) var text;
export (Texture) var icon;

onready var _label = $center/HBoxContainer/text;
onready var _icon = $center/HBoxContainer/icon;

func _ready() -> void:
	_label.text = text;
	_icon.texture = icon;
