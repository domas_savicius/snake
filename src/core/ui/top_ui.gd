extends Control

var _logger = Log.get_logger("top_ui.gd");

onready var _high_score_label = $MarginContainer/v_container/highscore/high_score;
onready var _score_label = $MarginContainer/v_container/score/score_value;
onready var _bonus_container = $MarginContainer/v_container/bonus;
onready var _bonus_time_label = $MarginContainer/v_container/bonus/time;
onready var _bonus_label = $MarginContainer/v_container/bonus/Label;

var _high_score_effect = preload("res://core/shared/effects/new_high_score_effect.tscn");

func _ready():
	Events.connect_game_started(self, "_on_game_started");
	Events.connect_score_changed(self, "_on_score_changed");
	Events.connect_bonus_food_tick(self, "_on_bonus_food_tick");
	Events.connect_bonus_food_spawned(self, "_on_bonus_food_spawned");
	Events.connect_bonus_food_ended(self, "_on_bonus_food_ended");
	Events.connect_load_high_score(self, "_on_load_high_score");
	Events.connect_new_high_score_reached(self, "_on_new_high_score_reached");

	_score_label.set_score(0);

func _on_load_high_score(high_score: int):
	_logger.info("Set high score %s" %high_score);
	_high_score_label.text = String(high_score);

func _on_bonus_food_ended():
	_hide_bonus();

func _on_game_started(_snake_array, _options):
	_hide_bonus();

func _on_score_changed(score):
	_score_label.set_score(score);

func _on_new_high_score_reached(_score):
	var _high_score_effect_instance = _high_score_effect.instance();
	_high_score_effect_instance.set_position(_score_label.get_position());
	_score_label.add_child(_high_score_effect_instance);

func _on_bonus_food_tick(time_left):
	_bonus_time_label.text = String(time_left);

func _on_bonus_food_spawned(_food):
	_show_bonus();

func _hide_bonus():
	_bonus_time_label.visible = false;
	_bonus_label.visible = false;

func _show_bonus():
	_bonus_time_label.visible = true;
	_bonus_label.visible = true;