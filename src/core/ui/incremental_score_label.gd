extends Label

class_name IncrementalScoreLabel

onready var _tween = $update_tween;

var _target;
var _score = 0;

func set_score(score):
	_tween.interpolate_method(self, "_update_score", _score, score, 0.5, Tween.TRANS_LINEAR, Tween.EASE_OUT);
	_tween.start();

func _update_score(value: int):
	_score = value;
	text = String(_score);