extends Node

class_name LevelOptions

var speed;
var level;
var obstacle_positions;

func _init(_speed, _level, _obstacle_positions):
	speed = _speed;
	level = _level;
	obstacle_positions = _obstacle_positions;